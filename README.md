# Usage guide

I used [Java-getting-started](https://github.com/heroku/java-getting-started) project as a template of web service. Also [this pipeline example](https://docs.gitlab.com/ee/ci/examples/test-scala-application.html) was taken as a basis of my pipeline. You can check the result following the link below.

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://testprogr.herokuapp.com)

## Creating project

I built project in GitLab, logged in with Git Bash and set my SSH-keys. Then I created a local repository and placed there sample project `java-getting-started`.

The next step - pushing sample project to remote.

## Creating pipeline and deploying to Heroku

For creating pipeline I added `.gitlab-ci.yml` file to project from GitLab CI/CD Examples ([Test and deploy a Scala application to Heroku](https://docs.gitlab.com/ee/ci/examples/test-scala-application.html)). To avoid mistakes, docker image was changed to ubuntu. I added Heroku authorization token to GitLab variables and then used it via the variable key $HEROKU_VAR.
